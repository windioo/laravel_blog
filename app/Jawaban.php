<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    // One to Many
    public function pertanyaan()
    {
        return $this->belongsTo('App\Pertanyaan');
    }
}
