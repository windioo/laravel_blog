<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;
use App\Profil;
use Auth;
use App\Tag;
use Illuminate\Support\Facades\App;

class PertanyaanController extends Controller
{
        public function __construct()
        {
         $this->middleware('auth')->except(['index','show']);   // jika ingin beberapa yg di auth menggunakan only([])
        }
        

        public function index()
        {
            // $pertanyaan = DB::table('pertanyaan')->get();
            $user = Auth::user();
            $pertanyaan = $user->pertanyaan;
            
            return view('pertanyaan/index', compact('pertanyaan'));
        }

        public function create()
        {
            return view('pertanyaan.create');
        }
        
        public function store(Request $request)
        {
                $request->validate([
                    'judul' => 'required|unique:pertanyaan',
                    'isi' => 'required',
                ]);
                // $query = DB::table('pertanyaan')->insert([
                //     "judul" => $request["judul"],
                //     "isi" => $request["isi"]
                // ]);

                // $pertanyaan = new Pertanyaan;
                // $pertanyaan->judul= $request['judul'];
                // $pertanyaan->isi = $request['isi'];
                // $pertanyaan->save(); // insert into pertanyaan () values ()

                //1. explode untuk mengubah request tags menjadi array
                //2. looping ke array tags tadi, untuk array penampung
                //3. setiap sekali looping melakukan pengecekan apakah tags sudah ada
                //4. kalo ada ambil id nya
                //5. kalo belum simpan dulu tags nya, lalu ambil id
                //6. tampung id di array penampung
                $tags_arr = explode(',',$request["tags"]);
                    
                    $tag_ids = [];
                    foreach($tags_arr as $tag_name){
                        // $tag = Tag::where("tag_name",$tag_name)->first();
                        // if($tag){
                        //     $tag_ids[]= $tag->id;
                        // }else{
                        //     $new_tag = Tag::create(["tag_name"=>$tag_name]);
                        //     $tag_ids[]= $new_tag->id;
                        // }

                        //Cara FirstOrCrate
                        $tag = Tag::firstOrCreate(['tag_name'=>$tag_name]);
                        $tag_ids[]= $tag->id;
                    }

                        // Menggunakan Mass Assignment
                    $pertanyaan =Pertanyaan::create([
                        "judul" => $request['judul'],
                        "isi" => $request['isi'],
                        "tanggal_dibuat" => $request['tanggal_dibuat'],
                        "user_id"=> Auth::user()->id
                    ]);
                    //Bisa juga mengisi user_id dengan cara dibawah
                    // $user = Auth::user();
                    // $user->pertanyaan()->save($pertanyaan);
                        
                        $pertanyaan->tags()->sync($tag_ids);

                return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Disimpan!');
            
        }

        public function show($id)
        {
            // $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first(); //first hanya satu data, kalau banyak menggunakan get()
            // // dd($pertanyaan);
            $pertanyaan= Pertanyaan::find($id);
            // dd($pertanyaan);
            return view('pertanyaan/show',compact('pertanyaan'));
        }
        public function edit($id)
        {
            // $pertanyaan = DB::table('pertanyaan')->where('id',$id)->first();
            $pertanyaan = Pertanyaan::find($id);
            return view('pertanyaan/edit', compact('pertanyaan'));
        }
        public function update(Request $request,$id)
        {
            $request->validate([
                    'judul' => 'required',
                    'isi' => 'required',
                ]);
            // $pertanyaan = DB::table('pertanyaan')
            //                 ->where('id',$id)
            //                 ->update([
            //                     'judul'=>$request['judul'],
            //                     'isi'=> $request['isi']
            //                 ]);

            $pertanyaan = Pertanyaan::where('id',$id)->update([
                    "judul"=>$request['judul'],
                    "isi"=>$request['isi']
            ]);
            return redirect('/pertanyaan')->with('success','Berhasil Di Ubah!');
        }
        public function destroy($id)
        {
            // $pertanyaan = DB::table('pertanyaan')->where('id',$id)->delete();
            $pertanyaan = Pertanyaan::destroy($id);
            return redirect('/pertanyaan')->with('success','Berhasil Di Dihapus!');
        }
    }
