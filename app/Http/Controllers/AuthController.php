<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('register');
    }

    public function welcome(Request $request){
        $fname = $request['firstname'];
        $lname  = $request['lastname'];
        return view('welcome', compact('fname','lname'));
     }

   
}
