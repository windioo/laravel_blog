<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = "pertanyaan";

    //Menggunakan Mass Asignment

    //Atrribute yang white list(boleh diisi)
    // protected $fillable = ["judul","isi","tanggal_dibuat","tanggal_diperbaharui","user_id"];
    
    //kebalikkan fillable(yang di blacklist), kalo kosong = semua boleh diisi
    protected $guarded = [];

    public function user(){
        return $this->belongsTo('App\User');
    }

    // // One to One
    public function jawaban()
    {
        return $this->hasOne('App\Jawaban');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag','pertanyaan_tag','pertanyaan_id','tag_id');
    }
}
