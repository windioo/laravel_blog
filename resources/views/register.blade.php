<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
</head>
<body>
    <div>
      <h1>Buat Account Baru!</h1>
      <h3>Sign Up Form</h3>
      <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label> <br />
        <br />
        <input type="text" name="firstname" /> <br />
        <br />
        <label>Last name:</label> <br />
        <br />
        <input type="text" name="lastname" /> <br />
        <br />
        <label>Gender:</label> <br />
        <br />
        <input type="radio" name="gender" value="0" /> Male <br />
        <input type="radio" name="gender" value="1" /> Female <br />
        <input type="radio" name="gender" value="2" /> Other <br />
        <br />
        <label>Nationality</label> <br />
        <br />
        <select name="nationality">
          <option value="Indonesia">Indonesia</option>
          <option value="Malaysia">Malaysia</option>
          <option value="Singapore">Singapore</option>
          <option value="Australia">Australia</option>
        </select>
        <br />
        <br />
        <label>Language Spoken:</label> <br />
        <br />
        <input type="checkbox" name="language" value="0" />Bahasa Indonesia
        <br />
        <input type="Checkbox" name="language" value="1" />English <br />
        <input type="checkbox" name="language" value="2" /> Other <br />
        <br />
        <label>Bio:</label> <br />
        <br />
        <textarea name="bio" cols="30" rows="10"></textarea> <br />
        <input type="submit" value="Sign Up" />
      </form>
   
</body>
</html>