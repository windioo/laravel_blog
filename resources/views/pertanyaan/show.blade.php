@extends('layouts.master')

@section('content')
<div class="mt-3 ml-3 mr-3">
    <div class="card card-primary">
     <div class="card-header with-border">
        <h3 class="card-title">Show Table</h3>
     </div>

     <div class="card-body"> 
        <p>Judul : {{ $pertanyaan->judul }}</p>
        <hr>
        <p>Pertanyaan : {{ $pertanyaan->isi }}</p>
        <hr>
        <p>Penulis :  {{$pertanyaan->user->name }}</p>
        <hr>
        <div>Tags : 
            @forelse($pertanyaan->tags as $tag )
               <button class="btn btn-primary btn-sm">{{ $tag->tag_name }}</button>
               @empty
               No Tags
            @endforelse
        </div>
     </div>
     
</div>
    <div class="box-footer text-right  ml-3 mr-3 mb-3">
                <a href="/pertanyaan" class="btn btn-primary">Kembali</a>
              </div>
    </div>
   
    
@endsection