@extends('layouts.master')

@section('content')
<div class="ml-3 mt-3">
 <div class="card card-primary ">
            <div class="card-header with-border ">
              <h3 class="card-title ">Ubah Pertanyaan </h3>
            </div>
            
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/pertanyaan/{{ $pertanyaan->id }}" method="POST">
                @csrf
                @method('PUT')
              <div class="box-body">
                <div class="form-group ml-3 mr-3 mt-3">
                  <label for="exampleInputEmail1">Judul</label>
                  <input type="text" class="form-control" id="judul" name="judul"  value="{{ old('judul', $pertanyaan->judul) }}" placeholder="Masukkan Judul">
                    @error('judul')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group ml-3 mr-3">
                  <label for="exampleInputPassword1">Isi</label>
                  <input class="form-control" rows="3" id="isi" name="isi" value="{{ old('isi', $pertanyaan->isi) }}" placeholder="Masukkan Pertanyaan"></input>
                  @error('isi')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                 <div class="form-group ml-3 mr-3 mt-3">
                  <label for="exampleInputEmail1">Tanggal Diperbaharui</label>
                  <input type="date"  id="tanggal_diperbaharui" name="tanggal_diperbaharui"  value="{{ old('tanggal_diperbaharui','') }}" >
                    @error('tanggal_diperbaharui')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right  ml-3 mr-3 mb-3">
                  <a href="/pertanyaan" class="btn btn-default ml-3">Kembali</a>
                <button type="submit" class="btn btn-primary ml-3">Submit</button>
              </div>
            </form>
          </div>
</div>
   
@endsection

