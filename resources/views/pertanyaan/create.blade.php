@extends('layouts.master')

@section('content')
<div class="ml-3 mt-3">
 <div class="card card-primary ">
            <div class="card-header with-border ">
              <h3 class="card-title ">Buat Pertanyaan</h3>
            </div>
            
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/pertanyaan" method="POST">
                @csrf
              <div class="box-body">
                <div class="form-group ml-3 mr-3 mt-3">
                  <label for="exampleInputEmail1">Judul</label>
                  <input type="text" class="form-control" id="judul" name="judul"  value="{{ old('judul','') }}" placeholder="Masukkan Judul">
                    @error('judul')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group ml-3 mr-3">
                  <label for="exampleInputPassword1">Isi</label>
                  <textarea class="form-control" rows="3" id="isi" name="isi" value="{{ old('isi','') }}" placeholder="Masukkan Pertanyaan"></textarea>
                  @error('isi')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                 <div class="form-group ml-3 mr-3 mt-3">
                  <label for="exampleInputEmail1">Tanggal Dibuat</label>
                  <input type="date"  id="tanggal_dibuat" name="tanggal_dibuat"  value="{{ old('tanggal_dibuat','') }}" >
                    @error('tanggal_dibuat')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group ml-3 mr-3 mt-3">
                  <label for="exampleInputEmail1">Tags</label>
                  <input type="text" class="form-control" id="tags" name="tags"  value="{{ old('tags','') }}" placeholder="Masukkan Tags">
                    @error('Tags')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right  ml-3 mr-3 mb-3">
                <a href="/pertanyaan" class="btn btn-default ml-3">Kembali</a>
                <button type="submit" class="btn btn-primary ml-3">Submit</button>
              </div>
            </form>
          </div>
</div>
   
@endsection

