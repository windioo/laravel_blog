@extends('layouts.master')

@section('content')
    <div class="mt-3 ml-3 mr-3">
        <div class="card card-primary">
            <div class="card-header with-border">
              <h3 class="card-title">Pertanyaan Table</h3>
            </div>
            <!-- /.box-header -->
            <div class="card-body">
              @if(session('success')) 
                <div class="alert alert-success">
                  {{ session('success') }}
                </div>
              @endif
              <a href="{{ route("pertanyaan.create") }}" class="btn btn-primary mt-2 mb-3">Buat Pertanyaan</a>
              <table class="table table-bordered">
                <thead class="bg-dark"><tr>
                  <th style="width: 10px">No</th>
                  <th>Judul</th>
                  <th>Isi</th>
                  <th style="width: 40px">Actions</th>
                </tr></thead>
                <tbody>
                @forelse ($pertanyaan as $key => $tanya)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $tanya->judul }}</td>
                    <td>{{ $tanya->isi }}</td>
                    <td style="display: flex">
                      <a href="/pertanyaan/{{ $tanya->id }}" class="btn btn-info btn-sm m-sm-1">Show</a>
                      <a href="/pertanyaan/{{ $tanya->id }}/edit" class="btn btn-success btn-sm m-sm-1">Edit</a>
                      <form action="/pertanyaan/{{ $tanya->id }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm m-sm-1">
                      </form>
                      
                    </td>
                </tr>    
                @empty
                <tr>
                  <td colspan ="4" align="center">Belum ada Pertanyaan</td>
                </tr>
                @endforelse
                </tbody>
              
               
              </table>
            </div>
            <!-- /.box-body -->
            
          </div>
    </div>
@endsection